#include "BasicExcel.h"
#include "ResultData.h"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
using namespace YExcel;
using namespace std;
#define RESULT "result.xls"
#define Lentoid 1
//#define X265    0
/*
**1.读取所有文件到内存；
**2.创建excel，并将读取的数据按照指定格式输出；
*/
vector<ResultData> getData()
{
	system("dir *.txt /a-d/b/s > tmp.dat");
	ifstream in;
	in.open("tmp.dat");
	vector<ResultData> results;
	while(!in.eof())
	{
		char file_name[200];
		in.getline(file_name,200);
		ifstream in_tmp;
		in_tmp.open(file_name);
		if(in_tmp.fail())
			continue;
		while(!in_tmp.eof())
		{
			string str,time_str;
			char tstr[200];
			float d,y_psnr,u_psnr,v_psnr,kbps,time;
			ResultData data;
			in_tmp>>str;
#if Lentoid
			if (str=="average:")
			{
				in_tmp>>y_psnr>>u_psnr>>v_psnr>>kbps;
				in_tmp>>tstr;//"kbps"
				in_tmp>>tstr;//bytes num
				in_tmp>>tstr;//"bytes"
				in_tmp>>tstr;//runtime:xxx.xxs
				sscanf(tstr,"runtime:%fs",&d);
				time = d;
#else
			if(str=="global:")
			{
				in_tmp>>tstr;//kb/s:
				in_tmp>>kbps;
				in_tmp>>tstr;//PSNR
				in_tmp>>tstr;//Mean:
				in_tmp>>tstr;
				sscanf(tstr,"Y:%f",&d);
				y_psnr = d;
				in_tmp>>tstr;
				sscanf(tstr,"U:%f",&d);
				u_psnr = d;
				in_tmp>>tstr;
				sscanf(tstr,"V:%f",&d);
				v_psnr = d;
				time = 0;
#endif
				data.time = time;
				data.kbps = kbps;
				data.y_psnr=y_psnr;
				data.u_psnr=u_psnr;
				data.v_psnr=v_psnr;
				strcpy(data.file_name,file_name);
				results.push_back(data);
				cout<<data.file_name<<endl;
				cout<<data.kbps<<"\t"<<data.y_psnr<<"\t"<<data.u_psnr<<"\t"<<data.v_psnr<<endl;
				break;
			}
		}
		in_tmp.close();
	}
	in.close();
	system("del tmp.dat");
	return results;
}
void writeDataToExcel( vector<ResultData> results )
{
	BasicExcel e;
	e.New(1);
	e.RenameWorksheet("Sheet1", "Test1");
	BasicExcelWorksheet* sheet = e.GetWorksheet("Test1");
	if (sheet)
	{
		int rows = 1;
		sheet->Cell(0,0)->SetString("file_name");
		sheet->Cell(0,1)->SetString("Kbps");
		sheet->Cell(0,2)->SetString("Y-PSNR");
		sheet->Cell(0,3)->SetString("U-PSNR");
		sheet->Cell(0,4)->SetString("V-PSNR");
		sheet->Cell(0,5)->SetString("Time");
		for(vector<ResultData>::const_iterator it = results.begin(); it < results.end(); it++,rows++)
		{
			ResultData data_t = *it;
			sheet->Cell(rows,0)->SetString(data_t.file_name);
			sheet->Cell(rows,1)->SetDouble(data_t.kbps);
			sheet->Cell(rows,2)->SetDouble(data_t.y_psnr);
			sheet->Cell(rows,3)->SetDouble(data_t.u_psnr);
			sheet->Cell(rows,4)->SetDouble(data_t.v_psnr);
			sheet->Cell(rows,5)->SetDouble(data_t.time);
		}
		e.SaveAs(RESULT);
	}
}
int main(int argc, char* argv[])
{
	vector<ResultData> results;
	results = getData();
	writeDataToExcel( results );
	cout<<"结果已保存至"<<RESULT<<endl;
	system("pause");
}