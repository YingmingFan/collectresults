class ResultData
{
public :
	char file_name[200];
	float kbps;
	float y_psnr;
	float u_psnr;
	float v_psnr;
	float time;
};